type User = {name:string, age: number, logName:() =>void ,jobs:string[]}

let user:User = {
    name:'Sarvar',
    age : 50,
    jobs: ['a','b'],
    logName():void {
        console.log(this.name)
    }
};


let user2:User = {
    name: 'Max',
    age: 30,
    jobs :['2'],
    logName(): void {
        console.log(this.name)
    }
};